#include "BigInt.h"
using namespace std;
BigInt::BigInt()
{
	head = NULL;
	end = NULL;
	lenth = 0;
}
BigInt::~BigInt()
{

}
void BigInt::fixlen()
{
	MyNode *p = head;
	int tmp = 0;
	bool fuhao = false;
	if (p->value == '-')
	{
		fuhao = true;
		p = p->next;
	}
	while (p->next)
	{
		if (p->value == '0')
		{
			p = p->next;
			head = head->next;
			head->pre = NULL;
		}
		else break;
	}
	head = p;
	if (p->value == '0')
	{
		fuhao = false;
	}
	head->pre = NULL;
	while (p)
	{
		tmp++;
		p = p->next;
	}
	lenth = tmp;
	if (fuhao == true)
	{
		head->pre = new MyNode('-');
		head->pre->next = head;
		head = head->pre;
	}
}
void BigInt::copy(BigInt temp)
{
	if (temp.head == NULL)
	{
		return;
	}
	MyNode *p = temp.head;
	lenth = temp.lenth;
	head = new MyNode(p->value);
	end = head;
	p = p->next;
	while (p)
	{
		MyNode *New = new MyNode(p->value);
		end->next = New;
		New->pre = end;
		end = end->next;
		p = p->next;
	}
}
void BigInt::copy(MyNode *temp, int a)//temp为end指针,a为lenth
{
	MyNode *p;
	p = temp;
	lenth = a;
	end = new MyNode(p->value);
	head = end;
	p = p->pre;
	while (p)
	{
		MyNode *New = new MyNode(p->value);
		head->pre = New;
		New->next = head;
		head = head->pre;
		p = p->pre;
	}
}
void BigInt::copy(MyNode *temp,MyNode *thead, int a=0)//temp为end指针,a为lenth
{
	MyNode *p;
	p = temp;
	lenth = a;
	end = new MyNode(p->value);
	head = end;
	p = p->pre;
	if (p == NULL)
	{
		return;
	}

	else if (p != thead)
	{
		while (p != thead)
		{
			MyNode *New = new MyNode(p->value);
			head->pre = New;
			New->next = head;
			head = head->pre;
			p = p->pre;
		} 

		MyNode *New = new MyNode(p->value);
		head->pre = New;
		New->next = head;
		head = head->pre;
	}
	else 
	{
		MyNode *New = new MyNode(p->value);
		head->pre = New;
		New->next = head;
		head = head->pre;
	}
}
bool BigInt::compare(BigInt temp)
{	
	MyNode *p = head;
	MyNode *t = temp.head;
	if (lenth > temp.lenth)
		return true;
	else if (lenth<temp.lenth)
		return false;
	else
	{
		while (p)
		{
			if (p->value > t->value)
				return true;
			else if (p->value < t->value)
				return false;
			else
			{
				p = p->next;
				t = t->next;
			}
		}
		return true;
	}
}
bool BigInt::compare(MyNode *a, MyNode *b, int a1, int b1)//传入head指针
{
	MyNode *p = a;
	MyNode *t = b;
	if (a1 > b1)
		return true;
	else if (a1 < b1)
		return false;
	else
	{
		while (p->next)
		{
			if (p->value > t->value)
				return true;
			else if (p->value < t->value)
				return false;
			else
				p = p->next;
		}
		return true;
	}
}
bool BigInt::equal(BigInt temp)
{
	MyNode *p = head;
	MyNode *t = temp.head;
	if (lenth != temp.lenth)
		return false;

	while (t)
	{
		if (p->value != t->value)
			return false;
		else
		{
			p = p->next;
			t = t->next;
		}
	}
	return true;
}
void BigInt::print()
{
	cout << "大数:";
	MyNode *p = head;
	bool fuhao = false;
	if (p == NULL)
	{
		cout << "NULL";
		return;
	}
	if (p->value == '-')
	{
		fuhao = true;
		p = p->next;
	}
	if (p == NULL)
	{
		cout << "NULL";
		return;
	}
	while (p->next)
	{
		if (p->value == 0)
			p = p->next;
		else break;
	}
	if (p->value == '0')
	{
		fuhao = false;
	}
	if (fuhao == true)
	{
		cout << '-';
	}
	while (p)
	{
		cout << p->value;
		p = p->next;
	}
	cout << endl;
}
void BigInt::setnum(string s)
{
	int tmp=s.length();
	head = new MyNode(s[0]);
	end = head;
	for (int i = 1; i < tmp; i++)
	{
		end->next = new MyNode(s[i]);
		end->next->pre = end;
		end = end->next;
	}
	fixlen();
}
void BigInt::addnum(char tmp)
{
	if (end)
	{
		end->next = new MyNode(tmp);
		end->next->pre = end;
		end = end->next;
	}
	else
	{
		head = new MyNode(tmp);
		end = head;
	}
	lenth++;
}
int BigInt::charadd(char a, char b)
{
	return a + b - 48 * 2;
}
int BigInt::caddi(char a, int b)
{
	return a + b - 48;
}
int BigInt::cjian(char a, char b)
{
	return a-b;
}
int BigInt::cjiani(char a, int b)
{
	return a-48-b;
}
int BigInt::cchengc(char a, char b)
{
	return((a - 48)*(b - 48));
}
void BigInt::add(BigInt a,BigInt b)//a,b为end指针.a1,b1为lenth
{	
	MyNode *ah;
	ah = a.head;
	MyNode *bh;
	bh = b.head;
	head = NULL;
	end = NULL;
	bool fuhao = false;
	MyNode *p1t=a.head;
	MyNode *p2t=b.head;
	if (a.head->value == '-' && b.head->value == '-')
	{
		fuhao = true;
		a.head = a.head->next;
		b.head = b.head->next;
		a.head -> pre = NULL;
		b.head -> pre= NULL;
	}
	if (a.end == NULL && b.end == NULL)
	{
		cout << "两个大数都为空";
		return;
	}
	else if (a.end == NULL && b.end != NULL)
	{
		copy(b);
		return;
	}
	else if (a.end != NULL&&b.end == NULL)
	{
		copy(a);
		return;
	}
	else if (a.head->value == '-'&&b.head->value != '-')
	{	
		a.head = a.head->next;
		a.head->pre= NULL;
		jian(b, a);
	}
	else if (a.head->value != '-'&&b.head->value == '-')
	{
		b.head = b.head->next;
		b.head->pre = NULL;
		jian(a,b);
		
	}
	else {
		MyNode *p1;
		MyNode *p2;
		int now;//当前相加值
		int jin = 0;//进位值
		int ge;//个位值
		if (a.lenth > b.lenth)//当a较长时
		{
			p1 = a.end;
			p2 = b.end;
			while (p2)
			{
				now = charadd(p1->value, p2->value) + jin;
				ge = now % 10;
				jin = now / 10;
				if (head == NULL)
				{
					end = new MyNode(ge + 48);
					head = end;
				}
				else
				{
					head->pre = new MyNode(ge + 48);
					head->pre->next = head;
					head = head->pre;
				}
				p1 = p1->pre;
				p2 = p2->pre;
			}


			while (p1)
			{
				now = caddi(p1->value, jin);
				head->pre = new MyNode(now % 10 + 48);
				head->pre->next = head;
				head = head->pre;
				jin = now / 10;
				if (now >= 10)
				{
					p1 = p1->pre;

				}
				else
				{
					p1 = p1->pre;
					break;
				}
			}
			if (p1 == NULL && jin > 0)
			{
				head->pre = new MyNode(jin + 48);
				head->pre->next = head;
				head = head->pre;
			}
			while (p1)
			{
				head->pre = new MyNode(p1->value);
				head->pre->next = head;
				head = head->pre;
				p1 = p1->pre;
			}

		}
		else if (a.lenth < b.lenth)//b要长
		{
			p2 = a.end;
			p1 = b.end;
			while (p2)
			{
				now = charadd(p1->value, p2->value) + jin;
				ge = now % 10;
				jin = now / 10;
				if (head == NULL)
				{
					end = new MyNode(ge + 48);
					head = end;
				}
				else
				{
					head->pre = new MyNode(ge + 48);
					head->pre->next = head;
					head = head->pre;
				}
				p1 = p1->pre;
				p2 = p2->pre;
			}


			while (p1)
			{
				now = caddi(p1->value, jin);
				head->pre = new MyNode(now % 10 + 48);
				head->pre->next = head;
				head = head->pre;
				jin = now / 10;
				if (now >= 10)
				{
					p1 = p1->pre;

				}
				else
				{
					p1 = p1->pre;
					break;
				}
			}
			if (p1 == NULL&& jin > 0)
			{
				head->pre = new MyNode(jin + 48);
				head->pre->next = head;
				head = head->pre;
			}
			while (p1)
			{
				head->pre = new MyNode(p1->value);
				head->pre->next = head;
				head = head->pre;
				p1 = p1->pre;
			}

		}
		else            //等长
		{
			p1 = a.end;
			p2 = b.end;
			while (p2)
			{
				now = charadd(p1->value, p2->value) + jin;
				ge = now % 10;
				jin = now / 10;
				if (head == NULL)
				{
					end = new MyNode(ge + 48);
					head = end;
				}
				else
				{
					head->pre = new MyNode(ge + 48);
					head->pre->next = head;
					head = head->pre;
				}
				p1 = p1->pre;
				p2 = p2->pre;
			}
			if (jin > 0)
			{
				head->pre = new MyNode(jin + 48);
				head->pre->next = head;
				head = head->pre;
			}
		}
	}
	if (fuhao == true)
	{
		a.head = p1t;
		b.head = p2t;
		head->pre = new MyNode('-');
		head->pre->next = head;
		head = head->pre;
	}
	if (ah->value == '-')
	{
		a.head = ah;
		a.head->next->pre = a.head;
	}
	if (bh->value == '-')
	{
		b.head = bh;
		b.head->next->pre = b.head;
	}
}
void BigInt::add(BigInt a)
{
	MyNode *ah;
	ah = a.head;
	MyNode *bh;
	bh =head;
	bool fuhao = false;
	MyNode *p1t = a.head;
	MyNode *p2t = head;
	if (a.head->value == '-' && head->value == '-')
	{
		fuhao = true;
		a.head = a.head->next;
		head = head->next;
		a.head->pre = NULL;
		head->pre = NULL;
	}
	if (a.end == NULL && end == NULL)
	{
		cout << "两个大数都为空";
		return;
	}
	else if (a.end == NULL && end != NULL)
	{
		return;
	}
	else if (a.end != NULL&&end == NULL)
	{
		copy(a);
		return;
	}
	else if (a.head->value == '-'&&head->value != '-')
	{
		a.head = a.head->next;
		a.head->pre = NULL;
		jian(*this, a);
	}
	else if (a.head->value != '-'&&head->value == '-')
	{
		head = head->next;
		head->pre = NULL;
		jian(a, *this);

	}
	else {
		MyNode *p1;
		MyNode *p2;
		MyNode *p3=NULL;
		int now;//当前相加值
		int jin = 0;//进位值
		int ge;//个位值
		if (a.lenth > lenth)//当a较长时
		{
			p1 = a.end;
			p2 = end;
			while (p2)
			{
				now = charadd(p1->value, p2->value) + jin;
				ge = now % 10;
				jin = now / 10;
				p2->value = ge+48;
				p1 = p1->pre;
				p3 = p2;
				p2 = p2->pre;
			}
			head = p3;

			while (p1)
			{
				now = caddi(p1->value, jin);
				head->pre = new MyNode(now % 10 + 48);
				head->pre->next = head;
				head = head->pre;
				jin = now / 10;
				if (now >= 10)
				{
					p1 = p1->pre;

				}
				else
				{
					p1 = p1->pre;
					break;
				}
			}
			if (p1 == NULL && jin > 0)
			{
				head->pre = new MyNode(jin + 48);
				head->pre->next = head;
				head = head->pre;
			}
			while (p1)
			{
				head->pre = new MyNode(p1->value);
				head->pre->next = head;
				head = head->pre;
				p1 = p1->pre;
			}

		}
		else if (a.lenth < lenth)//b要长
		{
			p2 = a.end;
			p1 = end;
			while (p2)
			{
				now = charadd(p1->value, p2->value) + jin;
				ge = now % 10;
				jin = now / 10;
				p1->value = ge+48;
				p1 = p1->pre;
				p2 = p2->pre;
			}

			while (p1)
			{
				now = caddi(p1->value, jin);
				p1->value = now % 10 + 48;
				jin = now / 10;
				if (now >= 10)
				{
					p3 = p1;
					p1 = p1->pre;

				}
				else
				{
					p3 = p1;
					p1 = p1->pre;
					break;
				}
			}
			head = p3;
			if (p1 == NULL&& jin > 0)
			{
				head->pre = new MyNode(jin + 48);
				head->pre->next = head;
				head = head->pre;
			}
			while (p1)
			{
				head->pre = new MyNode(p1->value);
				head->pre->next = head;
				head = head->pre;
				p1 = p1->pre;
			}

		}
		else            //等长
		{
			p1 = a.end;
			p2 = end;
			while (p2)
			{
				now = charadd(p1->value, p2->value) + jin;
				ge = now % 10;
				jin = now / 10;
				p2->value = ge+48;
				p1 = p1->pre;
				p2 = p2->pre;
			}
			if (jin > 0)
			{
				head->pre = new MyNode(jin + 48);
				head->pre->next = head;
				head = head->pre;
			}
		}
	}
	if (fuhao == true)
	{
		a.head = p1t;
		head = p2t;
		head->pre = new MyNode('-');
		head->pre->next = head;
		head = head->pre;
	}
	if (ah->value == '-')
	{
		a.head = ah;
		a.head->next->pre = a.head;
	}
	if (bh->value == '-')
	{
		head = bh;
		head->next->pre = head;
	}
}
void BigInt::jian(BigInt a,BigInt b)
{	
	MyNode *ah;
	ah = a.head;
	MyNode *bh;
	bh = b.head;
	head = NULL;
	end = NULL;
	if (a.end == NULL && b.end == NULL)
	{
		cout << "两个大数都为空";
		return;
	}
	else if (a.end == NULL && b.end != NULL)
	{
		copy(b);
		head->pre = new MyNode('-');
		head->pre->next = head;
		head = head->pre;
		return;
	}
	else if (a.end != NULL&&b.end == NULL)
	{
		copy(a);
		return;
	}
	else if (a.head->value != '-'&&b.head->value == '-')
	{
		b.head = b.head->next;
		b.head->pre = NULL;
		add(a, b);
	}
	else if (a.head->value == '-'&& b.head->value != '-')
	{
		a.head = a.head->next;
		a.head->pre = NULL;
		add(a, b);
		head->pre = new MyNode('-');
		head->pre->next = head;
		head = head->pre;
	}
	else if (a.head->value == '-'&& b.head->value == '-')
	{
		a.head = a.head->next;
		a.head->pre = NULL;
		b.head = b.head->next;
		b.head->pre = NULL;
		jian(b,a);
	}
	else
	{
		MyNode *p1;
		MyNode *p2;
		int bu=0;//补数
		int zhi;//最终值
		if (a.equal(b))
		{
			head = new MyNode('0');
			end = head;
			return;
		}
		else if (a.compare(b))//若a较大,结果为正数
		{
			p1 = a.end;
			p2 = b.end;
			while (p2)
			{
				zhi = p1->value - p2->value-bu;
				if(zhi < 0)
				{
					zhi = zhi + 10;
					bu = 1;
				}
				else
				{
					bu = 0;
				}
				if (end == NULL)
				{
					end = new MyNode(zhi+48);
					head = end;
				}
				else
				{
					head->pre = new MyNode(zhi + 48);
					head->pre->next = head;
					head = head->pre;
				}
				p1 = p1->pre;
				p2 = p2->pre;
			}
			if (p1 != NULL)
			{
				while (p1->pre)
				{
					zhi = p1->value - 48 - bu;
					if (zhi < 0)
					{
						zhi = zhi + 10;
						bu = 1;
					}
					else
					{
						bu = 0;
					}
					head->pre = new MyNode(zhi + 48);
					head->pre->next = head;
					head = head->pre;
					p1 = p1->pre;
				}
				if (p1->value == '1' && bu == 1)
				{
				}
				else
				{
					zhi = p1->value - bu;
					head->pre = new MyNode(zhi);
					head->pre->next = head;
					head = head->pre;
					p1 = p1->pre;
				}
			}
		}
		else//若b较大,结果为负数
		{
			bu = 0;
			p1 = b.end;
			p2 = a.end;
			while (p2)
			{
				zhi = p1->value - p2->value - bu;
				if (zhi < 0)
				{
					zhi = zhi + 10;
					bu = 1;
				}
				else
				{
					bu = 0;
				}
				if (end == NULL)
				{
					end = new MyNode(zhi + 48);
					head = end;
				}
				else
				{
					head->pre = new MyNode(zhi + 48);
					head->pre->next = head;
					head = head->pre;
				}
				p1 = p1->pre;
				p2 = p2->pre;
			}
			if (p1 != NULL)
			{
				while (p1->pre)
				{
					zhi = p1->value - 48 - bu;
					if (zhi < 0)
					{
						zhi = zhi + 10;
						bu = 1;
					}
					else
					{
						bu = 0;
					}
					head->pre = new MyNode(zhi + 48);
					head->pre->next = head;
					head = head->pre;
					p1 = p1->pre;
				}
				if (p1->value == '1' && bu == 1)
				{
				}
				else
				{
					zhi = p1->value - bu;
					head->pre = new MyNode(zhi);
					head->pre->next = head;
					head = head->pre;
					p1 = p1->pre;
				}
			}
			head->pre = new MyNode('-');
			head->pre->next = head;
			head = head->pre;
		}
	}
	if (ah->value == '-')
	{
		a.head = ah;
		a.head->next->pre = a.head;
	}
	if (bh->value == '-')
	{
		b.head = bh;
		b.head->next->pre = b.head;
	}
	fixlen();
}

void BigInt::cheng(BigInt a, BigInt b)
{	
	MyNode *ah;
	ah = a.head;
	MyNode *bh;
	bh = b.head;
	head = NULL;
	end = NULL;
	bool fuhao = false;
	if (a.head == NULL || b.head == NULL)
	{
		cout << "NULL";
		return;
	}
	else
	{
		if (a.head->value == '-'&&b.head->value != '-')//去'-'号
		{
			a.head = a.head->next;
			a.head->pre = NULL;
			fuhao = true;
		}
		else if (a.head->value != '-'&&b.head->value == '-')
		{
			b.head = b.head->next;
			b.head->pre = NULL;
			fuhao = true;
		}
		else if (a.head->value == '-'&&b.head->value == '-')
		{
			a.head = a.head->next;
			a.head->pre = NULL;
			b.head = b.head->next;
			b.head->pre = NULL;
		}
		else
		{

		}
		MyNode *p1;
		MyNode *p2;
		MyNode *pt;
		int jin=0;
		int zhi;
		int ge;
		int bu0=0;//补0
		if (a.lenth >= b.lenth)//p2指向短的一条链
		{
			
			p1 = a.end;
			p2 = b.end;
			pt = a.end;
		}
		else
		{
			p1 = b.end;
			p2 = a.end;
			pt = b.end;
		}
		while (p2)
		{	
			BigInt *tmp=new BigInt();
			jin = 0;
			while (p1)
			{
				zhi = cchengc(p1->value, p2->value)+jin;
				jin = zhi / 10;
				ge = zhi % 10;
				if (tmp->head == NULL)
				{
					tmp->end = new MyNode(ge + 48);
					tmp->head = tmp->end;
				}
				else
				{
					tmp->head->pre = new MyNode(ge + 48);
					tmp->head->pre->next = tmp->head;
					tmp->head = tmp->head->pre;
				}
				p1 = p1->pre;
			}
			if (jin > 0)
			{
				tmp->head->pre = new MyNode(jin + 48);
				tmp->head->pre->next = tmp->head;
				tmp->head = tmp->head->pre;
			}			
			for (int i = 0; i < bu0; i++)
			{
				tmp->end->next = new MyNode('0');
				tmp->end->next->pre = tmp->end;
				tmp->end = tmp->end->next;
			}
			tmp->fixlen();
			add(*tmp);
			bu0++;
			flash(tmp->head);
			p2 = p2->pre;
			p1 = pt;
		}

		if (fuhao == true)
		{
			head->pre = new MyNode('-');
			head->pre->next =head;
			head = head->pre;
		}
		if (ah->value == '-')
		{
			a.head = ah;
			a.head->next->pre = a.head;
		}
		if (bh->value == '-')
		{
			b.head = bh;
			b.head->next->pre = b.head;
		}
		fixlen();
	}
}



void BigInt::chu(BigInt a, BigInt b)
{
	head = NULL;
	end = NULL;
	bool fuhao = false;
	MyNode *ah = a.head;
	MyNode *bh = b.head;
	if (a.head->value == '-'&&b.head->value != '-')//去'-'号
	{
		a.head = a.head->next;
		a.head->pre = NULL;
		fuhao = true;
	}
	else if (a.head->value != '-'&&b.head->value == '-')
	{
		b.head = b.head->next;
		b.head->pre = NULL;
		fuhao = true;
	}
	else if (a.head->value == '-'&&b.head->value == '-')
	{
		a.head = a.head->next;
		a.head->pre = NULL;
		b.head = b.head->next;
		b.head->pre = NULL;
	}
	if (a.head->value == '0')
	{
		head = new MyNode('0');
		end = head;
		return;
	}
	if (b.head->value == '0')
	{
		cout << "除数为0,非法计算"<<endl;
		return;
	}
	if (a.equal(b))
	{
		head = new MyNode('1');
		end = head;
		
	}
	else if (a.compare(b))
	{
		MyNode *ph1;//a临时head
		MyNode *ph2;//b临时head
		MyNode *pe1;//a的临时end
		MyNode *pe2;//b的临时end
		BigInt *bei = new BigInt();//被除数
		
		ph1 = a.head;
		ph2 = b.head;
		pe1 = a.head;
		pe2 = b.head;
		for (int i = 0; i < b.lenth - 1; i++)
		{
			pe1 = pe1->next;
		}
		bei->copy(pe1, ph1);
		bei->fixlen();
		while (pe1)
		{
			BigInt *tmp2 = new BigInt();
			BigInt *tmp3 = new BigInt();
			char c = 49;
			for (;;)
			{
				if (c == 58)
				{
					break;
				}
				MyNode *node = new MyNode(c);
				BigInt *tmp = new BigInt();
				tmp->head = node;
				tmp->end = node;
				tmp->cheng(b, *tmp);
				if (bei->compare(*tmp))
				{
					c++;
				}
				else
				{
					flash(tmp->head);
					break;
				}
				flash(tmp->head);

			}

			if (c - 48 != 0)
			{
				if (head == NULL)
				{
					head = new MyNode(c - 1);
					end = head;
				}
				else
				{
					end->next = new MyNode(c - 1);
					end->next->pre = end;
					end = end->next;
				}
			}
			MyNode *node = new MyNode(c - 1);
			BigInt *tmp = new BigInt();
			tmp->head = node;
			tmp->end = node;
			tmp2->copy(*tmp);
			flash(tmp->head);
			tmp->cheng(b, *tmp2);
			flash(tmp2->head);
			tmp2=new BigInt();
			tmp2->copy(*bei);
			flash(bei->head);
			bei->jian(*tmp2 , *tmp);
			flash(tmp2->head);
			flash(tmp->head);

			if (pe1->next)
			{
				bei->end->next = new MyNode(pe1->next->value);
				bei->end->next->pre = bei->end;
				bei->end = bei->end->next;
			}
			bei->fixlen();
			pe1 = pe1->next;
		}
		flash(bei->head);
	}
	else
	{
		head = new MyNode('0');
		end = head;
		return;
	}

	if (fuhao == true)
	{
		head->pre = new MyNode('-');
		head->pre->next = head;
		head = head->pre;
	}
	if (ah->value == '-')
	{
		a.head = ah;
		a.head->next->pre = a.head;
	}
	if (bh->value == '-')
	{
		b.head = bh;
		b.head->next->pre = b.head;
	}
	fixlen();
}

void BigInt::flash(MyNode *p)//参数为head指针
{	
	
	if (p != NULL)
	{	
		MyNode *tmp;
		while (p)
		{
			tmp = p->next;
			delete p;
			p = tmp;
		}
	}
}
void BigInt::flash()
{
	head = NULL;
	end = NULL;
	lenth = 0;
}

void BigInt::zhishu(BigInt a, int b)
{	
	copy(a);
	
	for (int i = 0; i < b-1; i++)
	{	
		BigInt *tmp = new BigInt();
		tmp->copy(*this);
		flash(head);
		cheng(a,*tmp);
		flash(tmp->head);

	}
}

void BigInt::mod(BigInt a, BigInt b)
{
	head = NULL;
	end = NULL;
	bool fuhao = false;
	MyNode *ah = a.head;
	MyNode *bh = b.head;
	if (a.head->value == '-'&&b.head->value != '-')//去'-'号
	{
		a.head = a.head->next;
		a.head->pre = NULL;
		fuhao = true;
	}
	else if (a.head->value != '-'&&b.head->value == '-')
	{
		b.head = b.head->next;
		b.head->pre = NULL;
		fuhao = true;
	}
	else if (a.head->value == '-'&&b.head->value == '-')
	{
		a.head = a.head->next;
		a.head->pre = NULL;
		b.head = b.head->next;
		b.head->pre = NULL;
	}
	if (a.head->value == '0')
	{
		cout << "被除数为0,非法计算" << endl;
		return;
	}
	if (b.head->value == '0')
	{
		cout << "除数为0,非法计算" << endl;
		return;
	}
	if (a.equal(b))
	{
		head = new MyNode('0');
		end = head;
		lenth = 1;
		return;
	}
	else if (a.compare(b))
	{
		MyNode *ph1;//a临时head
		MyNode *ph2;//b临时head
		MyNode *pe1;//a的临时end
		MyNode *pe2;//b的临时end
		BigInt *bei = new BigInt();//被除数
		ph1 = a.head;
		ph2 = b.head;
		pe1 = a.head;
		pe2 = b.head;
		for (int i = 0; i < b.lenth - 1; i++)
		{
			pe1 = pe1->next;
		}
		bei->copy(pe1, ph1);
		bei->fixlen();
		while (pe1)
		{
			BigInt *tmp2 = new BigInt();
			char c = 49;
			for (;;)
			{
				if (c == 58)
				{
					break;
				}
				MyNode *node = new MyNode(c);
				BigInt *tmp = new BigInt();
				tmp->head = node;
				tmp->end = node;
				tmp->cheng(b, *tmp);
				if (bei->compare(*tmp))
				{
					c++;
				}
				else
				{
					flash(tmp->head);
					break;
				}
				flash(tmp->head);

			}
			MyNode *node = new MyNode(c - 1);
			BigInt *tmp = new BigInt();
			tmp->head = node;
			tmp->end = node;
			tmp2->copy(*tmp);
			flash(tmp->head);
			tmp->cheng(b, *tmp2);
			flash(tmp2->head);
			tmp2 = new BigInt();
			tmp2->copy(*bei);
			flash(bei->head);
			bei->jian(*tmp2, *tmp);
			flash(tmp2->head);
			flash(tmp->head);

			if (pe1->next)
			{
				bei->end->next = new MyNode(pe1->next->value);
				bei->end->next->pre = bei->end;
				bei->end = bei->end->next;
			}
			bei->fixlen();
			pe1 = pe1->next;
		}
		copy(*bei);
		flash(bei->head);
	}
	else
	{
		copy(a);
		return;
	}

	if (fuhao == true)
	{
		head->pre = new MyNode('-');
		head->pre->next = head;
		head = head->pre;
	}
	if (ah->value == '-')
	{
		a.head = ah;
		a.head->next->pre = a.head;
	}
	if (bh->value == '-')
	{
		b.head = bh;
		b.head->next->pre = b.head;
	}
	fixlen();
}

void BigInt::fin(int a)
{	
	string c1;
	if (a == 0)
	{
		ifstream OpenFile("E:\\a.txt");
		OpenFile >> c1;
		OpenFile.close();
	}
	else
	{
		ifstream OpenFile("E:\\b.txt");
		OpenFile >> c1;
		OpenFile.close();
	}
	setnum(c1);
}

void BigInt::fout()
{
	ofstream OpenFile("E:\\out.txt");
	MyNode *p = head;
	bool fuhao = false;
	if (p == NULL)
	{
		cout << "NULL";
		return;
	}
	if (p->value == '-')
	{
		fuhao = true;
		p = p->next;
	}
	if (p == NULL)
	{
		cout << "NULL";
		return;
	}
	while (p->next)
	{
		if (p->value == 0)
			p = p->next;
		else break;
	}
	if (p->value == '0')
	{
		fuhao = false;
	}
	if (fuhao == true)
	{
		OpenFile << '-';
	}
	while (p)
	{
		OpenFile << p->value;
		p = p->next;
	}
}
