#pragma once
#include"MyNode.h"
#include<string>
#include <fstream>
using namespace std;
class BigInt
{
public:
	BigInt();
	~BigInt();
	void fixlen();//去0并计算长度
	void copy(BigInt);//复制大整数
	void copy(MyNode *, int);
	void copy(MyNode * temp, MyNode * thead, int a);
	//利用end与lenth复制大整数
	bool compare(BigInt);//比较当前与参数大等(true)小(false)
	bool compare(MyNode * a, MyNode * b, int a1, int b1);
	//比较当前与参数大等(true)小(false)
	bool equal(BigInt);//是否相同
	void print();//打印大数
	void setnum(string);//为大数赋值
	void addnum(char);//添加数
	int charadd(char, char);//char类型相加
	int caddi(char, int);//char和int相加
	int cjian(char, char);//char类型相减
	int cjiani(char, int);//char减int
	int cchengc(char, char);//char乘char
	void add(BigInt,BigInt);//大数加法
	void add(BigInt);//大数自加法
	void jian(BigInt,BigInt);//大数减法
	void cheng(BigInt, BigInt);//大数乘法
	void cheng(BigInt);//大数乘法
	void chu(BigInt, BigInt);//大数除法
	void flash(MyNode *);//清理内存
	void flash();//初始化
	void zhishu(BigInt, int);//指数运算,只支持正数
	void mod(BigInt, BigInt);//取余,只支持正数
	void fin(int);//从文件读取0为a.txt，1为b.txt
	void fout();//输出至E:\out.txt
	MyNode *head;//头
	MyNode *end;//尾
	int lenth;//大整数实际长度
};



