#include"BigInt.h"
#include<ctime>
using namespace std;

int main()
{	
	string c1;
	string c2;
	BigInt a;
	BigInt b;
	BigInt c;
	int n;
	cout << "选择输入方式:" << endl;
	cout << "1.从文件读入" << endl;
	cout << "2.从控制台输入" << endl;
	cin >> n;
	if (n == 1)
	{
		a.fin(0);
		b.fin(1);
		a.print();
		b.print();
	}
	else
	{
		cout << "输入第一个大数:" << endl;
		cin >> c1;
		a.setnum(c1);
	}
	cout << endl;
	cout << "选择操作" << endl;
	cout << "1.加"<<endl;
	cout << "2.减" << endl;
	cout << "3.乘" << endl;
	cout << "4.除" << endl;
	cout << "5.乘方" << endl;
	cout << "6.取余" << endl;
	cin >> n;
	clock_t time=clock();
	if (n == 1)
	{
		cout << "输入第二个大数:" << endl;
		cin >> c2;
		b.setnum(c2);
		time = clock();
		c.add(a, b);
		cout << "加操作成功"<<endl;
	}
	else if (n == 2)
	{	
		cout << "输入第二个大数:" << endl;
		cin >> c2;
		b.setnum(c2);
		time = clock();
		c.jian(a,b);
		cout << "加操作成功" << endl;
	}
	else if (n == 3)
	{
		cout << "输入第二个大数:" << endl;
		cin >> c2;
		b.setnum(c2);
		time = clock();
		c.cheng(a, b);
		cout << "乘操作成功" << endl;
	}
	else if (n == 4)
	{
		cout << "输入第二个大数:" << endl;
		cin >> c2;
		b.setnum(c2);
		time = clock();
		c.chu(a, b);
		cout << "除操作成功" << endl;
	}
	else if (n == 5)
	{
		cout << "输入指数:"<<endl;
		int zhi;
		cin >> zhi;
		time = clock();
		c.zhishu(a, zhi);
		cout << "乘方操作成功" << endl;
	}
	else if (n == 6)
	{
		cout << "输入2的指数:" << endl;
		int zhi;
		cin >> zhi;
		c.head = new MyNode('2');
		c.end = c.head;
		c.zhishu(c, zhi);
		time = clock();
		c.mod(a, c);
		cout << "取余操作成功" << endl;
	}
	time = clock() - time;
	cout << "用时:" << time /1000<< "秒" <<time%1000<<"毫秒"<< endl;
	cout << endl;
	cout << "选择输出方式:" << endl;
	cout << "1.输出至文件E:\\out.txt"<<endl;
	cout << "2.输出至控制台" << endl;
	cout << "3.同时输出" << endl;
	cin >> n;
	if (n == 1)
	{
		c.fout();
	}
	else if (n == 2)
	{
		c.print();
	}
	else
	{
		c.fout();
		c.print();
	}
	
	system("pause");
}